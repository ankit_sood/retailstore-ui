import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebarnav',
  templateUrl: './sidebarnav.component.html',
  styleUrls: ['./sidebarnav.component.css']
})
export class SidebarnavComponent implements OnInit {
  showNav: boolean = true;
  
  constructor() { }

  ngOnInit() {
  }

  toggleSidebar(){
    //$('#sidebar').toggleClass('active');
  }
}
