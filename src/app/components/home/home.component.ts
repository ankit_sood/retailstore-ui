import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  showSidePanel: boolean = true;
  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() : void{
    if(this.showSidePanel){
      document.getElementById("sidebar").className='active';
    }else{
      document.getElementById("sidebar").className='';
    }
    this.showSidePanel = !this.showSidePanel;
  }
}
